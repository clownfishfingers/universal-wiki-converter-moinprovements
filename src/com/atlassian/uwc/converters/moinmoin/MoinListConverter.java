package com.atlassian.uwc.converters.moinmoin;

import java.util.Enumeration;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.uwc.converters.BaseConverter;
import com.atlassian.uwc.ui.Page;

public class MoinListConverter extends BaseConverter {
	
	protected static final String LSEP = System.getProperty("line.separator", "\n");
	

	
	public void convert(Page page) {	
		String cont = page.getOriginalText();
		
		page.setConvertedText(this.convertList(cont));

	}
	

	
	private class Indent {
		
		int level;
		String symbol;
		
		Indent(int level, String symbol){
			this.level = level;
			this.symbol = symbol;
		}
		
		Indent(){
		}
		
		Indent getNew(int level, String symbol){
			return new Indent(level, symbol);
		}
		
		boolean isGreater(int level){
			return level > this.level;
		}
		
		boolean isSame(int level){
			return level == this.level;
		}		
		boolean isLess(int level){
			return level < this.level;
		}

		public String toString() {
			return "level=" + level + ", symbol=" + symbol;
		}
	}
	
	private final Indent first = new Indent(-1," ");
	
	void appendX(StringBuilder sb, Stack<Indent> s){
		for (Enumeration<Indent> e = s.elements(); e.hasMoreElements(); ){
			sb.append( e.nextElement().symbol );
		}
	}
	
	static Pattern startp = Pattern.compile("(\\s*)(([\\*\\.])|([AaIi1]\\.)) +.*", Pattern.MULTILINE);
	static Pattern continuation = Pattern.compile("(\\s*)([a-zA-Z0-9].*)", Pattern.MULTILINE);
	
	String convertList(String input){
		StringBuilder output = new StringBuilder();

		final Stack<Indent> indent_stack = new Stack<Indent>();
		indent_stack.push(first);
		
				
		for( String line : input.split("\\r?\\n") ){
			
			
			
			Matcher mat = startp.matcher(line);
			Matcher mat_cont = continuation.matcher(line);
			
			if( mat.matches() ){
				
				int indentation_level =  mat.group(1) != null ? mat.group(1).length() : 0;
				
				
				// find the different type
				final int symbolLength;
				final String symbol;
				
				if( mat.group(4) != null ){
					symbol = "#";
					symbolLength = 2;
				} else if( mat.group(3) != null) {
					symbol = "*";
					symbolLength = 1;
				} else { // somehow the line matches but the enum items are not there
					output.append(line);
					output.append(LSEP);

					// reset the indentation
					indent_stack.clear();
					indent_stack.push(first);
					continue;
				}
				
				Indent i = indent_stack.peek();
				
				
				if( i.isGreater(indentation_level) ) {
					i = i.getNew(indentation_level, symbol);
					indent_stack.push(i);

				} else if ( i.isLess(indentation_level) ){
					/* Need to dedent, but by how many?
					* e.g.
					*
					*  * L1
					*   * L2
					*    * L3
					*  * L1 - back by 2!
					*/

					while (i.isLess(indentation_level)) {
						i = indent_stack.pop();
						i = indent_stack.peek();
					}
					
					if( i.equals(first) ){
						// Moin Moin has a silly behavior of something like this
						//    * first (level one)
						//  * second level one (this has to be level one but only if they dont have a previous thing 
						output.append(LSEP);
						i = i.getNew(1, symbol);
						indent_stack.push(i);
					}
				}
				
				i.symbol = symbol; //actual current symbol 
				
				appendX(output, indent_stack);
				output.append(line.substring(indentation_level + symbolLength));
				output.append(LSEP);
			}
			else {
				output.append(line);
				output.append(LSEP);

				// reset the indentation_level
				indent_stack.clear();
				indent_stack.push(first);
			}
			
			
		}
		
		return output.toString();
	}

}
