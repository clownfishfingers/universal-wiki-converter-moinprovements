package com.atlassian.uwc.converters.moinmoin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.uwc.converters.BaseConverter;
import com.atlassian.uwc.converters.tikiwiki.RegexUtil;
import com.atlassian.uwc.ui.Page;

public class FullRelativeLinkConverter extends BaseConverter {

	public void convert(Page page) {
		String input = page.getOriginalText();
		String converted = convertLinks(input, page.getName());
		page.setConvertedText(converted);
	}

	Pattern relativelink = Pattern.compile("\\[\\[" +
			"(\\/)?" +
			"([^\\]]*)" +
			"\\]\\]");
	protected String convertLinks(String input, String title) {
		Matcher linkFinder = relativelink.matcher(input);
		if (title.endsWith(".txt")) { //if we end with .txt, get rid of it
			title = title.replaceAll("\\.txt$", ""); 
		}
		StringBuffer sb = new StringBuffer();
		boolean found = false;
		while (linkFinder.find()) {
			found = true;
			String leading_slash = linkFinder.group(1);
			String linkcontent = linkFinder.group(2);
			//check for alias
			String alias = "";
			if (linkcontent.contains("|")) {
				String[] parts = linkcontent.split("\\|");
				linkcontent = parts[0];
				alias = parts[1];
			}
			if ("/".equals(leading_slash)) {
				linkcontent = title + " " + linkcontent; //create a full relative link. 
			}
			// The link content might still have slashes - the original code only works for one level down
			// If it's a URL, we don't want to touch the slashes though
			if (! linkcontent.contains("://")) {
				linkcontent = linkcontent.replaceAll("/", " ");
			}
			String replacement = ("".equals(alias))?
					"[" + linkcontent + "]": //no alias
					"[" + alias + "|" + linkcontent + "]" ; //has alias
			replacement = RegexUtil.handleEscapesInReplacement(replacement);
			linkFinder.appendReplacement(sb, replacement);
		}
		if (found) {
			linkFinder.appendTail(sb);
			return sb.toString();
		}
		return input;
	}

}
